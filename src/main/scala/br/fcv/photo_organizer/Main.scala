package br.fcv.photo_organizer

import java.text.SimpleDateFormat
import java.util.Date
import scalax.file.Path.string2path
import scalax.file.Path
import scalax.file.PathSet
import scalax.file.FileSystem
import scalax.file.PathMatcher.IsFile
import scala.annotation.tailrec
import scopt.OptionParser

object Main extends App {   
    
    private object config {
        var resources: List[String] = Nil
        var targetDir: Option[String] = None
        var dirPattern: String = "yyyy-MM-dd"
        var verbose: Boolean = false
    }
    
    val parser = new OptionParser("photo-organizer") {
        opt("v", "verbose", "set as verbose mode", { config.verbose = true })
        opt("t", "target-directory", "target diretory where files will moved into", { d: String => config.targetDir = Some(d) })
        opt("dirname-pattern", "format of new created dir", { d: String => config.dirPattern = d })
        arglist("<dir> ...", "directories to be processed", { d: String => config.resources = config.resources :+ d })
    }
    
    if (parser.parse(args)) {
        for (resource <- config.resources) {            
            log("- processing resource: " + resource)
	    	val path = Path(resource)
		
			val images = path.children ("*.{jpeg,jpg}").collect {case IsFile(file) => file}	
            
            if (images.isEmpty)
                log("empty resource")
			
			val formatter = new SimpleDateFormat(config.dirPattern)
			
			for (image <- images) {
			    val modifiedDate = new Date(image.lastModified);		
				val strModifiedDate = formatter format modifiedDate
				
				val newDir = path / strModifiedDate
				newDir.createDirectory(failIfExists = false)
				
				val original = newDir / image.name;
				val filename = ensureUnique(original, original, None)
				log("from '%s' -> '%s'".format(image, filename))
				// image.moveTo(target=filename)
			}
        }
    }
	
	@tailrec
	private def ensureUnique(original: Path, p: Path, index: Option[Int]): Path = {
	    if (!p.exists) {
	        p
	    } else {
	        val currentIndex = index.getOrElse(0)
	        val newName = original.extension match {
	            case Some(ext) => {
	                val root = original.parent.get	                
	                root / (original.simpleName + "." + currentIndex + "." + ext)
	            }
	            case None => {
	                val root = original.parent.get
	                root / (original.name + '.' + currentIndex)
	            }
	        }	        
	        ensureUnique(original, newName, Some( currentIndex + 1 ))
	    }
	}
	
	private def log(msg: => String) {
	    if (config.verbose)
	        println(msg)
	}
	
}
